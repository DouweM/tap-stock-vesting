# tap-stock-vesting

`tap-stock-vesting` is a Singer tap for stock vesting plans.

Built with [Singer SDK](https://gitlab.com/meltano/singer-sdk).

## Usage with Meltano

You can easily run `tap-stock-vesting` by itself or in a pipeline using [Meltano](https://www.meltano.com/).

### Installation

1. [Install Meltano](http://meltano.com/docs/getting-started.html#install-meltano):

    ```bash
    pip install meltano
    ```

3. [Create a project](http://meltano.com/docs/getting-started.html#create-your-meltano-project):

    ```bash
    meltano init tap-stock-vesting-playground
    cd tap-stock-vesting-playground
    ```

2. Add `tap-stock-vesting` to `meltano.yml` under `plugins`/`extractors` as a custom plugin:

    ```yaml
    plugins:
      extractors:
      - name: tap-stock-vesting
        namespace: tap_stock_vesting
        pip_url: git+https://gitlab.com/DouweM/tap-stock-vesting.git
        settings:
        - name: stocks
          kind: array
    ```

3. Install `tap-stock-vesting`:

    ```bash
    meltano install extractor tap-stock-vesting
    ```

### Configuration

Add `stocks` to the `tap-stock-vesting` definition in `meltano.yml` under `config`:

```yaml
plugins:
  extractors:
  - name: tap-stock-vesting
    # ...
    config:
      stocks:
      - name: Startup
        current_price: 100.00
        grants:
        - name: Initial
          start_date: "2019-01-01"
          # vesting_months: 48
          # cliff_months: 12
          amount: 100000
          repurchase_price: 0.10
        - name: Refresher
          start_date: "2020-01-01"
          amount: 10000
          repurchase_price: 1.00
        - name: Promotion
          start_date: "2021-01-01"
          amount: 1000
          repurchase_price: 10.00
          # strike_price: 10.00 # If not exercised yet
```

### Usage

#### Extract only

Run `tap-stock-vesting` by itself using `meltano invoke`:

```bash
meltano invoke tap-stock-vesting
```

#### Extract & Load

Run `tap-stock-vesting` in a pipeline with a loader using `meltano elt`:

```bash
# Add target-jsonl loader
meltano add loader target-jsonl

# Create output directory
mkdir output

# Run pipeline
meltano elt tap-stock-vesting target-jsonl

# View results
cat output/assets.jsonl
```

You should see output along the following lines:

```json
{"name": "Startup Initial (Vested)", "value": 5416666.667}
{"name": "Startup Initial (Unvested)", "value": 4583.333333}
{"name": "Startup Refresher (Vested)", "value": 291666.6667}
{"name": "Startup Refresher (Unvested)", "value": 7083.333333}
{"name": "Startup Promotion (Vested)", "value": 0}
{"name": "Startup Promotion (Unvested)", "value": 10000}
```

## Stand-alone usage

### Installation

```bash
pip install git+https://gitlab.com/DouweM/tap-stock-vesting.git
```

### Configuration

Create a `config.json` file with content along the following lines:

```json
{
  "stocks": [
    {
      "name": "Startup",
      "current_price": 100.0,
      "grants": [
        {
          "name": "Initial",
          "start_date": "2019-01-01",
          "amount": 100000,
          "repurchase_price": 0.1
        },
        {
          "name": "Refresher",
          "start_date": "2020-01-01",
          "amount": 10000,
          "repurchase_price": 1.0
        },
        {
          "name": "Promotion",
          "start_date": "2021-01-01",
          "amount": 1000,
          "repurchase_price": 10.0
        }
      ]
    }
  ]
}
```

### Usage

Run `tap-stock-vesting` with the `config.json` file created above:

```bash
tap-stock-vesting --config config.json
```
