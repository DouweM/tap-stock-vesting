"""Stock vesting tap class."""

from pathlib import Path
from typing import List
import click
from singer_sdk import Tap, Stream
from singer_sdk.helpers.typing import (
    ArrayType,
    BooleanType,
    DateTimeType,
    IntegerType,
    NumberType,
    ObjectType,
    PropertiesList,
    Property,
    StringType,
)

from tap_stock_vesting.streams import (
    AssetsStream,
)

PLUGIN_NAME = "tap-stock-vesting"

STREAM_TYPES = [
    AssetsStream,
]


class TapStockVesting(Tap):
    """Stock vesting tap class."""

    name = "tap-stock-vesting"
    config_jsonschema = PropertiesList(
        Property(
            "stocks",
            ArrayType(
                ObjectType(
                    Property("name", StringType, required=True),
                    Property("current_price", NumberType, required=True),
                    Property(
                        "grants",
                        ArrayType(
                            ObjectType(
                                Property("name", StringType, required=True),
                                Property("start_date", DateTimeType, required=True),
                                Property("amount", NumberType, required=True),
                                Property("repurchase_price", NumberType),  # Default: 0
                                Property("vesting_months", NumberType),  # Default: 48
                                Property("cliff_months", NumberType),  # Default: 12
                            )
                        ),
                        required=True,
                    ),
                )
            ),
            required=True,
        ),
        Property("start_date", DateTimeType),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


# CLI Execution:

cli = TapStockVesting.cli

if __name__ == "__main__":
    cli()
