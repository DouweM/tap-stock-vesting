"""Stream class for tap-stock-vesting."""

import decimal

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from pathlib import Path
from typing import Any, Dict, List, Optional, Iterable

from singer_sdk import Tap, Stream
from singer_sdk.helpers.typing import (
    ArrayType,
    BooleanType,
    DateTimeType,
    IntegerType,
    NumberType,
    ObjectType,
    PropertiesList,
    Property,
    StringType,
)


class TapStockVestingStream(Stream):
    pass


class AssetsStream(TapStockVestingStream):
    name = "assets"

    schema = PropertiesList(
        Property("name", StringType, required=True),
        Property("value", NumberType, required=True),
    ).to_dict()
    primary_keys = ["name"]

    def __init__(self, tap: Tap):
        super().__init__(tap=tap, name=None, schema=None)

    @property
    def partitions(self) -> List[dict]:
        return [
            {
                "name": " ".join([stock["name"], grant["name"]]),
                "start_date": grant["start_date"],
                "vesting_months": grant.get("vesting_months", 48),
                "cliff_months": grant.get("cliff_months", 12),
                "amount": grant["amount"],
                "repurchase_price": grant.get("repurchase_price", 0),
                "strike_price": grant.get("strike_price", 0),
                "current_price": stock["current_price"],
            }
            for stock in self.config.get("stocks", [])
            for grant in stock.get("grants", [])
        ]

    def get_records(self, partition: dict) -> Iterable[dict]:
        name = partition["name"]

        start_date = datetime.fromisoformat(partition["start_date"]).date()
        vested_ratio = self._vested_ratio(
            start_date, partition["vesting_months"], partition["cliff_months"]
        )

        decimal.getcontext().prec = 10

        amount = decimal.Decimal(partition["amount"])
        amount_vested = amount * decimal.Decimal(str(vested_ratio))
        amount_unvested = amount - amount_vested

        current_price = decimal.Decimal(partition["current_price"])

        strike_price = decimal.Decimal(partition["strike_price"])
        if strike_price > 0:
            vested_value = amount_vested * (current_price - strike_price)
            yield {"name": name, "value": vested_value}

            return

        vested_value = amount_vested * current_price
        yield {"name": f"{name} (Vested)", "value": vested_value}

        repurchase_price = decimal.Decimal(partition["repurchase_price"])
        if repurchase_price > 0:
            unvested_value = amount_unvested * repurchase_price
            yield {"name": f"{name} (Unvested)", "value": unvested_value}

    def _vested_ratio(self, start_date, vesting_months, cliff_months):
        today = date.today()

        cliff_date = start_date + relativedelta(months=cliff_months)
        if today < cliff_date:
            return 0.0

        end_date = start_date + relativedelta(months=vesting_months)
        if today >= end_date:
            return 1.0

        vested_delta = relativedelta(today, start_date)
        vested_months = vested_delta.years * 12 + vested_delta.months
        return vested_months / vesting_months
