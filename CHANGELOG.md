# Changelog

## v0.0.2

- [#1](https://gitlab.com/DouweM/tap-stock-vesting/-/issues/1) Support stock options that haven't been exercised (early) yet with `strike_price` property

## v0.0.1

Initial release
